from django.shortcuts import render
from django.contrib.auth import logout
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic import TemplateView, ListView
from rest_framework.response import Response
from rest_framework.serializers import ModelSerializer
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from rest_framework.generics import ListAPIView, UpdateAPIView, RetrieveUpdateDestroyAPIView, CreateAPIView, \
    ListCreateAPIView
from rest_framework.parsers import FileUploadParser
from django.http import QueryDict
from django.contrib.auth.models import (User, Group, Permission)
from datetime import datetime, timedelta, date
from .models import (city, product_category, place, seller, product, product_image, order_items, shop_category,
                     seller_image,
                     cart, order, customer_details, product_qty, deliver_charge, advertisement, sale_product,
                     product_detail)
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import IsAuthenticated
from .serializers import (CitySerializer, CategorySerializer, ShopSerializer, ProductSerializer, SellerCatSerializer,
                          ProductImageSerializer, UserRegisterSerializer, ChangePasswordSerializer, UserSerializer,
                          AdvSerializer, allSellerSerializer)
from rest_framework.pagination import PageNumberPagination
from rest_framework.filters import SearchFilter
from django.db.models import Q
import json


class SetPagination(PageNumberPagination):
    page_size = 100

    def get_paginated_response(self, data):
        return Response({
            'status': "200",
            'next': self.get_next_link(),
            'previous': self.get_previous_link(),
            'count': self.page.paginator.count,
            'total_pages': self.page.paginator.num_pages,
            'results': data
        })


class getCities(ListAPIView):
    # permission_classes = (IsAuthenticated,)

    queryset = city.objects.all().order_by('order_id')
    serializer_class = CitySerializer
    pagination_class = SetPagination
    filter_backends = [SearchFilter]
    search_fields = ['full_name', 'district__full_name']


class getCategories(ListAPIView):
    # permission_classes = (IsAuthenticated,)

    pagination_class = SetPagination

    def post(self, request, *args, **kwargs):
        city_id = request.data.get('city_id')
        queryset1 = sale_product.objects.filter(city__id=city_id).values_list('product_category', flat=True).distinct()
        queryset = product_category.objects.filter(pk__in=list(queryset1)).order_by('order_id')
        if not queryset:
            return Response({'status': 404})
        serializer = CategorySerializer(queryset, many=True)
        page = self.paginate_queryset(serializer.data)
        return self.get_paginated_response(page)


'''    
class getshops(ListAPIView):
    #permission_classes = (IsAuthenticated,)
    
    pagination_class = SetPagination
    
    def post(self, request, *args, **kwargs):
        city_id=request.data.get('city_id')
        queryset1=sale_product.objects.filter(city__id=city_id).values_list('seller', flat=True).distinct()
        queryset = seller.objects.filter(pk__in=list(queryset1)).order_by('order_id')
        if not queryset:
            return Response({'status':404})
        serializer = ShopSerializer(queryset,many=True)
        page = self.paginate_queryset(serializer.data)
        return self.get_paginated_response(page)
'''


class getProducts(ListAPIView):
    # permission_classes = (IsAuthenticated,)

    pagination_class = SetPagination

    def post(self, request, *args, **kwargs):
        category_id = request.data.get('category')
        shop_id = request.data.get('shop_id')
        product_id = request.data.get('product_id')
        product_search = request.data.get('product_search')
        if product_search and shop_id:
            queryset = sale_product.objects.filter(
                Q(seller__id=shop_id, product__full_name__icontains=product_search) | Q(seller__id=shop_id,
                                                                                        product__short_name__icontains=product_search)).order_by(
                'order_id')
            if not queryset:
                return Response({'status': 404, "message": "no data"})
            serializer = ProductSerializer(queryset, many=True)
            page = self.paginate_queryset(serializer.data)
            return self.get_paginated_response(page)
        if product_id:
            queryset = sale_product.objects.filter(product__id=product_id).order_by('order_id')
            if not queryset:
                return Response({'status': 404})
            serializer = ProductSerializer(queryset, many=True)
            all_qty = product_qty.objects.filter(product_price__product__id=product_id)
            list_value = []
            for i in all_qty:
                list_value.append(
                    {'id': i.id, 'qty_type': i.qty_type, 'qty_number': i.qty_number, 'selled_price': i.selled_price,
                     'original_price': i.original_price})
            newdict = {'item': list_value}
            newdict.update({'product': serializer.data})
            return Response(newdict)
        if category_id and shop_id:
            queryset = sale_product.objects.filter(seller__id=shop_id, product_category__id=category_id).order_by(
                'order_id')
            if not queryset:
                return Response({'status': 404})
            serializer = ProductSerializer(queryset, many=True)
            page = self.paginate_queryset(serializer.data)
            return self.get_paginated_response(page)
        if not category_id and shop_id:
            print("else")
            queryset = sale_product.objects.filter(seller__id=shop_id).order_by('order_id')
            if not queryset:
                return Response({'status': 404})
            serializer = ProductSerializer(queryset, many=True)
            page = self.paginate_queryset(serializer.data)
            return self.get_paginated_response(page)
        if category_id and not shop_id:
            queryset = sale_product.objects.filter(product_category__id=category_id).order_by('order_id')
            if not queryset:
                return Response({'status': 404})
            serializer = ProductSerializer(queryset, many=True)
            page = self.paginate_queryset(serializer.data)
            return self.get_paginated_response(page)


class addToCart(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        try:
            product_id = request.data.get('product_id')
            item_quantity = request.data.get('item_quantity')
            item_id = request.data.get('item_id')
            if_present = cart.objects.filter(customer=request.user, product__id=product_id, status=True,
                                             product_qty__id=item_id)
            if if_present:
                cart.objects.filter(customer=request.user, product__id=product_id, status=True,
                                    product_qty__id=item_id).update(qty=item_quantity)
            else:
                item_id = product_qty.objects.get(id=item_id)
                prd = product.objects.get(id=product_id)
                cart.objects.create(customer=request.user, product=prd, qty=item_quantity, product_qty=item_id)
            return Response({"response": "Added Successfully", "status": "200", })
        except Exception as e:
            return Response({'status': '404', 'message': str(e)})


class viewCart(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        try:
            all_cart = cart.objects.filter(customer=request.user, status=True)
            if all_cart:
                datas = []
                for i in all_cart:
                    img = product_image.objects.filter(product__id=i.product.id)
                    total = float(i.qty) * float(i.product_qty.selled_price)
                    datas.append({'cart_id': i.id, 'items_id': i.product_qty.id, 'product_id': i.product.id,
                                  'product_name': i.product.full_name, 'product_image': img[0].image.url,
                                  'shop_name': i.product_qty.product_price.seller.name.first_name,
                                  'shop_id': i.product_qty.product_price.seller.id,
                                  'offer_price': i.product_qty.selled_price,
                                  'actual_price': i.product_qty.original_price, 'qty': i.qty, 'total': total,
                                  'items_type': i.product_qty.qty_type, 'items_number': i.product_qty.qty_number})
                return Response({'status': 200, 'data': datas})
            else:
                return Response({'status': '404', 'message': "no data"})
        except Exception as e:
            return Response({'status': '404', 'message': str(e)})


class removeFroCart(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        cart_id = request.data.get('cart_id')
        try:
            cart.objects.filter(id=cart_id).delete()
            return Response({"response": "removed Successfully", "status": "200"})
        except Exception as e:
            return Response({'status': '404', 'message': str(e)})


class buynow(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        cart_id = request.data.get('cart_id')
        address = request.data.get('address')
        latitude = request.data.get('latitude')
        longitude = request.data.get('longitude')
        try:
            new_order = order.objects.create(address=address, latitude=latitude, longitude=longitude, is_active=True,
                                             customer=request.user)
            for i in cart_id:
                cart_data = cart.objects.get(id=i)
                product_data = product.objects.get(id=cart_data.product.id)
                product_qty_data = product_qty.objects.get(id=cart_data.product_qty.id)
                order_items.objects.create(product=product_data, product_qty=product_qty_data, qty=cart_data.qty,
                                           order=new_order)
                cart.objects.filter(id=i).delete()
            return Response({"response": "order Successfully", "status": "200"})
        except Exception as e:
            return Response({'status': '404', 'message': str(e)})


class register(CreateAPIView):
    def post(self, request, format=None):
        serializer = UserRegisterSerializer(data=request.data)
        data = {}
        if serializer.is_valid():
            account = serializer.save()
            account.set_password(request.data.get('password'))
            account.save()
            group, created = Group.objects.get_or_create(name=request.data.get('type'))
            account.groups.add(group)
            token, created = Token.objects.get_or_create(user=account)
            data = {'status': '200', 'message': 'registered Successfully', 'token': token.key}
        else:
            reason = []
            try:
                reason.append(serializer.errors.get('email')[0])
            except Exception as e:
                pass
            try:
                reason.append(serializer.errors.get('username')[0])
            except Exception as e:
                pass
            data = {'Status': '0', 'Error': reason}
        return Response(data)


class login(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        if serializer.is_valid():
            user = serializer.validated_data['user']
            token, created = Token.objects.get_or_create(user=user)
            return Response({
                'status': '200',
                'username': user.username,
                'user_id': user.id,
                'phone': user.username,
                'token': token.key,
                'name': user.first_name,
                'type': str(user.groups.values_list('name', flat=True)[0])
            })
        else:
            return Response({'status': '404', 'error': "Username or password does not match"})


class UpdatePassword(APIView):
    """
    An endpoint for changing password.
    """
    permission_classes = (IsAuthenticated,)

    def get_object(self, queryset=None):
        return self.request.user

    def put(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = ChangePasswordSerializer(data=request.data)

        if serializer.is_valid():
            # Check old password
            old_password = serializer.data.get("old_password")
            if not self.object.check_password(old_password):
                return Response({'message': "old_password Wrong.", 'status': 404})
            # set_password also hashes the password that the user will get
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            return Response({"message": "changed Successfully", "status": "200"})
        return Response({'message': "This password is too common.", 'status': 404})


class profile_view(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        try:
            datas = customer_details.objects.get(customer__username=request.user)
            result = {
                'status': 200,
                'name': datas.customer.first_name,
                'username': datas.customer.username,
                'user_id': datas.customer.id,
                'email': datas.customer.email,
                'image': ''

            }
            if datas.image:
                result.update({'image': datas.image.url})
            return Response(result)
        except Exception as e:
            return Response({'status': '404', 'message': str(e)})


class UserUpdateView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def put(self, request, format=None):
        serializer = self.serializer_class(request.user, data=request.data)

        if serializer.is_valid():
            serializer.save()
            image = request.FILES.get('image')
            if image:
                print("yess")
                user = customer_details.objects.get(customer=request.user)
                user.image = image
                user.save()
            data = {"status": "200", "message": "updated success"}
            return Response(data)
        else:
            reason = serializer.errors.values()
            data = {"Status": "200", "Error": reason}
            return Response(data)


class delivery_charge(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        place = request.data.get('place_id')
        try:
            charge = deliver_charge.objects.get(city__id=place).charge
            return Response({"status": 200, "amount": str(charge)})
        except Exception as e:
            return Response({'status': '404', 'message': str(e)})


class advert(APIView):

    def post(self, request, *args, **kwargs):
        place_id = request.data.get('place_id')
        try:
            advrt = advertisement.objects.filter(city__id=place_id)
            serializer = AdvSerializer(advrt, many=True)
            return Response({"status": 200, "data": serializer.data})
        except Exception as e:
            return Response({'status': '404', 'message': str(e)})


class top_products(ListAPIView):
    pagination_class = SetPagination

    def post(self, request, *args, **kwargs):
        place_id = request.data.get('place_id')
        try:
            querysets = product.objects.filter(top_product=True, place__id=place_id).order_by('top_rank')
            if not querysets:
                return Response({'status': 404})
            serializer = ProductSerializer(querysets, many=True)
            page = self.paginate_queryset(serializer.data)
            return self.get_paginated_response(page)
        except Exception as e:
            return Response({'status': '404', 'message': str(e)})


class search_product(ListAPIView):
    # permission_classes = (IsAuthenticated,)

    pagination_class = SetPagination

    def post(self, request, *args, **kwargs):
        place_id = request.data.get('place_id')
        search_text = request.data.get('search_text')
        if search_text:
            queryset = sale_product.objects.filter(
                Q(product__full_name__icontains=search_text, city__id=place_id) | Q(city__id=place_id,
                                                                                    product__short_name__icontains=search_text)).order_by(
                'order_id')
            if not queryset:
                return Response({'status': 404, 'message': "no data"})
            serializer = ProductSerializer(queryset, many=True)
            page = self.paginate_queryset(serializer.data)
            return self.get_paginated_response(page)


class sellerView(ListAPIView):
    serializer_class = SellerCatSerializer
    queryset = shop_category.objects.all()


class getshops(ListAPIView):
    # permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        city_id = request.data.get('city_id')
        queryset = shop_category.objects.all()
        result = []
        for i in queryset:
            data = {'id': str(i.id), 'short_name': str(i.short_name), 'full_name': str(i.full_name),
                    'is_active': str(i.is_active), 'date_created': str(i.date_created), 'image': str(i.image),
                    'shop': ''}
            seller_data = seller.objects.filter(shop_category__id=i.id, city__id=city_id)
            shop_list = []
            for j in seller_data:
                img = seller_image.objects.filter(seller__id=j.id).order_by('-id')
                all_image = []
                for u in img:
                    all_image.append(u.image.url)
                data1 = {'id': j.id, 'name': j.name.first_name, 'image': all_image}
                shop_list.append(data1)
            data.update({'shop': shop_list})
            result.append(data)
        result1 = []
        for y in result:
            if (y['shop'] == []):
                pass
            else:
                result1.append(y)
        return Response({'data': result1})


class myorder(ListAPIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        all_orders = order_items.objects.filter(order__customer=self.request.user)
        result = []
        if all_orders:
            for i in all_orders:
                if i.order_now == 0:
                    status = 'processing'
                if i.order_now == 1:
                    status = 'confirmed'
                if i.order_now == 2:
                    status = 'picked'
                if i.order_now == 3:
                    status = 'on the way'
                if i.order_now == 4:
                    status = 'delivered'
                data = {
                    'id': str(i.id),
                    'status': status,
                    'address': str(i.order.address),
                    'product_name': str(i.product.full_name),
                    'qty_type': str(i.product_qty.qty_type),
                    'selled_price': str(i.product_qty.selled_price),
                    'original_price': str(i.product_qty.original_price),
                    'shop': str(i.product_qty.product_price.seller.name.first_name),
                    'qty_number': str(i.qty)
                }
                result.append(data)
            return_data = {'status': 1, 'data': result}
        else:
            return_data = {'status': 0, 'message': 'no data'}

        return Response(return_data)


class shopProductList(ListAPIView):

    def post(self, request, *args, **kwargs):
        result = []
        seller_id = request.data.get('seller_id')
        sale_products = sale_product.objects.filter(seller__id=seller_id)
        for i in sale_products:
            qty_type = product_qty.objects.get(product_price__id=i.id)
            image = product_image.objects.filter(product__id=i.product.id)
            if image:
                image = image[0].image
            data = {
                'qty_type': str(qty_type.qty_type),
                'product_name': str(i.product.short_name),
                'id': str(i.id),
                'original_price': str(qty_type.original_price),
                'selled_price': str(qty_type.selled_price),
                'totalStock': str(i.quantity),
                'shop': str(i.seller.name.first_name),
                'image': str(image)
            }
            result.append(data)

        print('dataaaa', result)
        return_data = {'status': 1, 'data': result}
        return Response(return_data)


class updateShopProductList(ListAPIView):

    def post(self, request, *args, **kwargs):
        print('kkkkkkkkkkkkk')
        sale_product_id = request.data.get('id')
        qty_type = request.data.get('qty_type')
        product_name = request.data.get('product_name')
        original_price = request.data.get('original_price')
        shopId = request.data.get('shopId')
        selled_price = request.data.get('selled_price')
        totalStock = request.data.get('totalStock')
        image = request.data.get('image')
        print('sale_product_id', sale_product_id)
        if sale_product_id:
            sale_products = sale_product.objects.filter(seller__id=sale_product_id)
            if sale_products:
                sale_products.update(quantity=totalStock)
                product_obj = product.objects.filter(id=sale_products[0].product.id).update(short_name=product_name)
                qty_type = product_qty.objects.filter(product_price__id=sale_products[0].id).update(qty_type=qty_type,
                                                                                                    selled_price=selled_price,
                                                                                                    original_price=original_price)
                if image:
                    image_update = product_image.objects.filter(product__id=sale_products[0].product.id)
                    image_update.update(image=image)
                return_data = {'status': 200, 'message': 'Updated Successfully'}
                return Response(return_data)
            else:
                return_data = {'status': 500, 'message': 'No such product....'}
                return Response(return_data)

        else:
            # print('elseeeee')
            # product_obj = product.objects.create(short_name=product_name,full_name=product_name,)
            # sale_product.objects.create(quantity=totalStock,
            #                             product=product,
            #                             product_stock=product_stock,
            #                             seller=seller,
            #                             city=city,
            #                             product_category=product_category,
            #                             is_active=True,
            #                             user_created=user_created,
            #                             user_modified=user_modified,
            #                             date_modified=date_modified)
            return_data = {'status': 200, 'message': 'Created Successfully'}
            return Response(return_data)


class removeProduct(ListAPIView):

    def post(self, request, *args, **kwargs):
        try:
            product_id = request.data.get('product_id')
            sale_products = sale_product.objects.get(product__id=product_id)
            sale_products.delete()
            return_data = {'status': 1, 'msg': 'Product Removed Successfully...'}
            return Response(return_data)
        except:
            return_data = {'status': 0, 'msg': 'Failed to remove product...'}
            return Response(return_data)


class orderlist(ListAPIView):

    def post(self, request, *args, **kwargs):
        result = []
        userId = request.data.get('userId')
        items = order_items.objects.filter(product_qty__product_price__seller__name__id=userId)

        for i in items:

            qty_type = product_qty.objects.get(product_price__id=i.product.id)
            image = product_image.objects.filter(product__id=i.product.id)
            all_image = []
            for u in image:
                all_image.append(u.image.url)

            data = {
                'orderId': i.id,
                'customer_name': str(i.order.customer.username),
                'item': str(i.product.short_name),
                'quantity': i.qty,
                'qtyType': qty_type.qty_type,
                'originalPrice': qty_type.original_price,
                'sellingPrice': qty_type.selled_price,
                'status': str(i.get_order_now_display()),
                'address': str(i.order.address),
                'image': all_image,
            }
            result.append(data)
        print('dataaaa', result)
        return_data = {'status': 1, 'data': result}
        return Response(return_data)


class updateStatus(ListAPIView):

    def post(self, request, *args, **kwargs):
        try:
            orderId = request.data.get('orderId')
            status = request.data.get('status')
            order = order_items.objects.filter(id=orderId)
            order.update(order_now=status)
            return_data = {'status': 1, 'msg': 'Order status updated...'}
            return Response(return_data)
        except Exception as e:
            print('errr', str(e))
            return_data = {'status': 0, 'msg': 'Failed to update status...'}
            return Response(return_data)



class add_product_under_shop(APIView):
    def post(self, request, *args, **kwargs):
        try:
            short_name = request.data.get('short_name')
            full_name = request.data.get('full_name')
            category_id = request.data.get('category_id')
            quantity = request.data.get('quantity')
            seller_id = request.data.get('seller_id')
            city_id = request.data.get('city_id')
            description = request.data.get('description')
            remarks = request.data.get('remarks')
            image = request.FILES.get('product_image')
            image_name = request.data.get('image_name')
            product_qty_details = request.data.get('product_qty_details')
            categ_details = product_category.objects.get(id=category_id)
            seller_details = seller.objects.get(name__id=seller_id)
            userID = User.objects.get(id=seller_id)

            prd_details = product.objects.create(short_name=short_name, full_name=full_name, category=categ_details,
                                                 is_public=True, user_created=userID)
            city_details = city.objects.get(id=city_id)
            sale_connected = sale_product.objects.create(quantity=quantity, product=prd_details, seller=seller_details,
                                                         city=city_details,
                                                         product_category=categ_details, user_created=userID)
            descr_product = product_detail.objects.create(description=description, remarks=remarks, product=prd_details,
                                                          user_created=userID)
            print(image)
            det_image = product_image.objects.create(name=image_name, product=prd_details)
            det_image.image = image
            det_image.save()

            x = json.loads(product_qty_details)
            for i in x:
                # print(i)
                product_qty.objects.create(qty_type=i['qty_type'], qty_number=i['qty_number'],
                                           selled_price=i['selled_price'], original_price=
                                           i['original_price'], product_price=sale_connected, user_created=userID)
            return Response({'status': 1})
        except Exception as e:
            return Response({'status': '0', 'message': str(e)})


class getSeller(ListAPIView):
    def post(self, request, *args, **kwargs):
        city_id = request.data.get('city_id')

        queryset = seller.objects.filter(city__id=city_id)
        if not queryset:
            return Response({'status': 404})
        serializer = allSellerSerializer(queryset, many=True)
        page = self.paginate_queryset(serializer.data)

        print('errr'," str(e)")

        return self.get_paginated_response(page)


class getAllCategories(ListAPIView):

    pagination_class = SetPagination

    def post(self, request, *args, **kwargs):
        queryset = product_category.objects
        if not queryset:
            return Response({'status': 404})
        serializer = CategorySerializer(queryset, many=True)
        page = self.paginate_queryset(serializer.data)
        return self.get_paginated_response(page)
