from .models import (product_category, place, seller, product, product_image, customer_details, shop_category,
                     product_qty, advertisement, city, sale_product, seller_image, product_detail, customer_details)
from rest_framework import serializers
from django.contrib.auth import get_user_model

User = get_user_model()

from django.contrib.auth.password_validation import validate_password


class ChangePasswordSerializer(serializers.Serializer):
    """
    Serializer for password change endpoint.
    """
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

    def validate_new_password(self, value):
        validate_password(value)
        return value


class CitySerializer(serializers.ModelSerializer):
    district = serializers.SerializerMethodField('is_district')

    def is_district(self, city):
        dist = city.district
        return str(dist)

    class Meta:
        model = city
        fields = ["id", "short_name", "full_name", "district"]


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = product_category
        fields = ["id", "short_name", "full_name", "image"]


class ShopSerializer(serializers.ModelSerializer):
    shop_images = serializers.SerializerMethodField('is_shop_images')

    def is_shop_images(self, seller):
        img = seller_image.objects.filter(seller=seller).order_by('-id')
        all_image = []
        for u in img:
            all_image.append(u.image.url)
        return all_image

    class Meta:
        model = seller
        fields = ["id", "name", "shop_images"]

    name = serializers.SerializerMethodField('get_name')

    def get_name(self, obj):
        return obj.name.first_name


class ProductSerializer(serializers.ModelSerializer):
    quantityType = serializers.SerializerMethodField('is_quantityType')
    quantityNumber = serializers.SerializerMethodField('is_quantityNumber')
    quantity_type_id = serializers.SerializerMethodField('is_quantity_type_id')
    original_price = serializers.SerializerMethodField('is_original_price')
    selled_price = serializers.SerializerMethodField('is_selled_price')
    product_images = serializers.SerializerMethodField('is_product_images')
    category_img = serializers.SerializerMethodField('is_category_img')
    description = serializers.SerializerMethodField('is_description')

    def is_quantityType(self, obj):
        is_value = product_qty.objects.filter(product_price=obj)
        return is_value[0].qty_type

    def is_quantityNumber(self, obj):
        is_value = product_qty.objects.filter(product_price=obj)
        return is_value[0].qty_number

    def is_quantity_type_id(self, obj):
        is_value = product_qty.objects.filter(product_price=obj)
        return is_value[0].id

    def is_original_price(self, obj):
        is_value = product_qty.objects.filter(product_price=obj)
        return is_value[0].original_price

    def is_selled_price(self, obj):
        is_value = product_qty.objects.filter(product_price=obj)
        return is_value[0].selled_price

    def is_product_images(self, obj):
        img = product_image.objects.filter(product=obj.product).order_by('-id')
        all_image = []
        for u in img:
            all_image.append(u.image.url)
        return all_image

    def is_category_img(self, obj):
        return obj.product_category.image.url

    def is_description(self, obj):
        return str(product_detail.objects.get(product__id=obj.product.id))

    class Meta:
        model = sale_product
        fields = ["id", "short_name", "full_name", "description", "category", "seller", "quantityType",
                  "quantityNumber", "quantity_type_id", "original_price", "selled_price", "product_images",
                  "category_img"]

    category = serializers.SerializerMethodField('get_category')
    seller = serializers.SerializerMethodField('get_seller')
    short_name = serializers.SerializerMethodField('get_short_name')
    full_name = serializers.SerializerMethodField('get_full_name')
    id = serializers.SerializerMethodField('get_id')

    def get_category(self, obj):
        return obj.product_category.full_name

    def get_seller(self, obj):
        return obj.seller.name.first_name

    def get_short_name(self, obj):
        return obj.product.short_name

    def get_full_name(self, obj):
        return obj.product.full_name

    def get_id(self, obj):
        return obj.product.id


class ProductImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = product_image
        fields = ["image", "product"]


class CustomerDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = customer_details
        fields = ["customer", "phone"]


class UserRegisterSerializer(serializers.ModelSerializer):
    customer_details = CustomerDetailsSerializer()

    class Meta:
        model = User
        fields = ["username", "email", "first_name", "password", "customer_details"]

    def create(self, validated_data):
        subject = validated_data.pop('customer_details')
        user = User.objects.create(**validated_data)
        customer_details.objects.create(customer=user, **subject)
        return user


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["username", "email", "first_name"]


class AdvSerializer(serializers.ModelSerializer):
    class Meta:
        model = advertisement
        fields = '__all__'


class allShopSerializer(serializers.ModelSerializer):
    shop_images = serializers.SerializerMethodField('is_shop_images')

    def is_shop_images(self, seller):
        img = seller_image.objects.filter(seller=seller).order_by('-id')
        all_image = []
        for u in img:
            all_image.append(u.image.url)
        return all_image

    class Meta:
        model = seller
        fields = '__all__'

    name = serializers.SerializerMethodField('get_name')

    def get_name(self, obj):
        return obj.name.first_name


class SellerCatSerializer(serializers.ModelSerializer):
    category_of_shops = allShopSerializer(read_only=True, many=True)

    class Meta:
        model = shop_category
        fields = '__all__'


class allSellerSerializer(serializers.ModelSerializer):
    shop_images = serializers.SerializerMethodField('is_shop_images')

    def is_shop_images(self, seller):
        img = seller_image.objects.filter(seller=seller).order_by('-id')
        all_image = []
        for u in img:
            all_image.append(u.image.url)
        return all_image

    class Meta:
        model = seller
        fields = '__all__'

    name = serializers.SerializerMethodField('get_name')

    def get_name(self, obj):
        return obj.name.first_name
