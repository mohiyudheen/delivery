from django.db import models
from django.contrib.auth.models import User,Permission

# Create your models here.
class nation(models.Model):
    short_name = models.CharField(max_length=255, blank=True,null=True)
    full_name = models.CharField(max_length=255, blank=True,null=True)
    is_active = models.BooleanField(default=True)  
    user_created = models.ForeignKey(User,on_delete=models.CASCADE,related_name='user_created_nation')
    date_created = models.DateTimeField(auto_now_add=True)
    user_modified = models.ForeignKey(User,blank=True,null=True,on_delete=models.CASCADE,related_name='user_modified_nation')
    date_modified = models.DateTimeField(blank=True,null=True)
    
    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.short_name
    
class state(models.Model):
    short_name = models.CharField(max_length=255, blank=True,null=True)
    full_name = models.CharField(max_length=255, blank=True,null=True)
    nation=models.ForeignKey(nation, blank=True, null=True, on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True) 
    user_created = models.ForeignKey(User,on_delete=models.CASCADE,related_name='user_created_state')
    date_created= models.DateTimeField(auto_now_add=True)
    user_modified = models.ForeignKey(User,blank=True,null=True,on_delete=models.CASCADE,related_name='user_modified_state')
    date_modified =models.DateTimeField(blank=True,null=True)
    
    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.short_name
    
class district(models.Model):
    short_name = models.CharField(max_length=255, blank=True,null=True)
    full_name = models.CharField(max_length=255, blank=True,null=True)
    state=models.ForeignKey(state, blank=True, null=True, on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True) 
    order_id=models.IntegerField(blank=True,null=True)
    user_created = models.ForeignKey(User,on_delete=models.CASCADE,related_name='user_created_district')
    date_created = models.DateTimeField(auto_now_add=True)
    user_modified = models.ForeignKey(User,blank=True,null=True,on_delete=models.CASCADE,related_name='user_modified_district')
    date_modified = models.DateTimeField(blank=True,null=True)
    
    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.full_name
    
class city(models.Model):
    short_name = models.CharField(max_length=255, blank=True,null=True)
    full_name = models.CharField(max_length=255, blank=True,null=True)
    district=models.ForeignKey(district, blank=True, null=True, on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True) 
    order_id=models.IntegerField(blank=True,null=True)
    user_created = models.ForeignKey(User,on_delete=models.CASCADE,related_name='user_created_city')
    date_created = models.DateTimeField(auto_now_add=True)
    user_modified = models.ForeignKey(User,blank=True,null=True,on_delete=models.CASCADE,related_name='user_modified_city')
    date_modified = models.DateTimeField(blank=True,null=True)
    
    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.short_name
    
    
class place(models.Model):
    short_name = models.CharField(max_length=255, blank=True,null=True)
    full_name = models.CharField(max_length=255, blank=True,null=True)
    city=models.ForeignKey(city, blank=True, null=True, on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True) 
    user_created = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE,related_name='user_add_place')
    date_created = models.DateField(auto_now_add=True)
    order_id=models.IntegerField(blank=True,null=True)
    user_modified = models.ForeignKey(User,blank=True,null=True,on_delete=models.CASCADE,related_name='user_modified_place')
    date_modified = models.DateTimeField(blank=True,null=True)
    
    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.short_name
    
class deliver_charge(models.Model):
    city=models.ForeignKey(city, blank=True, null=True, on_delete=models.CASCADE)
    charge=models.FloatField(blank=True,null=True)
    
    class Meta:
        default_permissions = ()

    def __str__(self):
        return str(self.charge)
    
class shop_category(models.Model):
    short_name = models.CharField(max_length=255, blank=True,null=True)
    full_name = models.CharField(max_length=255, blank=True,null=True)
    is_active = models.BooleanField(default=True) 
    user_created = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE,related_name='user_add_category_shop')
    date_created = models.DateField(auto_now_add=True)
    image=models.ImageField(upload_to ='shopcategory/images/')
    
    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.full_name
    
class seller(models.Model):
    name = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE,related_name='user')
    is_active = models.BooleanField(default=True)
    city = models.ForeignKey(city,on_delete=models.CASCADE, related_name='city_seller_shop')
    user_created = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE,related_name='user_seller_shop')
    date_created = models.DateField(auto_now_add=True)
    order_id=models.IntegerField(blank=True,null=True)
    shop_category=models.ForeignKey(shop_category,related_name='category_of_shops',blank=True, null=True, on_delete=models.CASCADE)
  
    
    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.name.username
    
class seller_details(models.Model):
    address = models.CharField(max_length=150)
    gst_number = models.CharField(max_length=15)
    seller = models.ForeignKey(seller,on_delete=models.CASCADE, related_name='seller_details_shop')
    place = models.ForeignKey(place,on_delete=models.CASCADE, related_name='place_seller_shop')
    is_active = models.BooleanField(default=True) 
    user_created = models.ForeignKey(User,on_delete=models.CASCADE,related_name='user_created_seller_details')
    date_created = models.DateTimeField(auto_now_add=True)
    user_modified = models.ForeignKey(User,blank=True, null=True,on_delete=models.CASCADE,related_name='user_modified_seller_details')
    date_modified = models.DateTimeField(blank=True, null=True)

    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.seller.name
    
class seller_image(models.Model):
    name = models.CharField(max_length=255, blank=True,null=True)
    title = models.CharField(max_length=100, blank=True,null=True)
    image_type = models.CharField(max_length=255, blank=True,null=True)
    image_size = models.CharField(max_length=255, blank=True,null=True)
    is_active = models.BooleanField(default=True)
    seller= models.ForeignKey(seller, blank=True, null=True, on_delete=models.CASCADE)
    date_created = models.DateField(auto_now_add=True)
    image=models.ImageField(upload_to ='seller/images/')
  
    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.seller.name.first_name


class product_category(models.Model):
    short_name = models.CharField(max_length=255, blank=True,null=True)
    full_name = models.CharField(max_length=255, blank=True,null=True)
    is_active = models.BooleanField(default=True) 
    user_created = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE,related_name='user_add_product_category')
    date_created = models.DateField(auto_now=True)
    image=models.ImageField(upload_to ='category/images/')
    order_id=models.IntegerField(blank=True,null=True)
    
    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.short_name
    
class product_sub_category(models.Model):
    short_name = models.CharField(max_length=255, blank=True,null=True)
    full_name = models.CharField(max_length=255)
    product_category = models.ForeignKey(product_category,on_delete=models.CASCADE, related_name='product_sub_category')
    is_active = models.BooleanField(default=True) 
    user_created = models.ForeignKey(User,on_delete=models.CASCADE,related_name='user_created_product_sub_category')
    date_created = models.DateTimeField(auto_now_add=True)
    user_modified = models.ForeignKey(User,blank=True, null=True,on_delete=models.CASCADE,related_name='user_modified_product_sub_category')
    date_modified =models.DateTimeField(blank=True, null=True)
    image=models.ImageField(upload_to ='subcategory/images/')
    order_id=models.IntegerField(blank=True,null=True)
    
    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.full_name
    
class product(models.Model):
    short_name = models.CharField(max_length=255, blank=True,null=True)
    full_name = models.CharField(max_length=255, blank=True,null=True)
    category=models.ForeignKey(product_category, blank=True, null=True, on_delete=models.CASCADE)
    product_sub_category = models.ForeignKey(product_sub_category, blank=True, null=True,on_delete=models.CASCADE, related_name='product_sub_category')
    is_active = models.BooleanField(default=True)
    order_id=models.IntegerField(blank=True,null=True)
    is_public= models.BooleanField(default=False) 
    user_created = models.ForeignKey(User,on_delete=models.CASCADE,related_name='user_created_product_model')
    date_created = models.DateTimeField(auto_now_add=True)
    user_modified = models.ForeignKey(User,blank=True, null=True,on_delete=models.CASCADE,related_name='user_modified_product_model')
    date_modified = models.DateTimeField(blank=True, null=True)
    
    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.short_name
    
class product_stock(models.Model):
    quantity = models.CharField(max_length=150)
    product = models.ForeignKey(product,on_delete=models.CASCADE, related_name='product_stock')
    seller = models.ForeignKey(seller,on_delete=models.CASCADE, related_name='seller_stock')
    is_active = models.BooleanField(default=True) 
    user_created = models.ForeignKey(User,on_delete=models.CASCADE,related_name='user_created_stock')
    date_created = models.DateTimeField(auto_now_add=True)
    user_modified = models.ForeignKey(User,blank=True, null=True,on_delete=models.CASCADE,related_name='user_modified_stock')
    date_modified = models.DateTimeField(blank=True, null=True)

    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.product.name
        
class product_detail(models.Model):
    description = models.TextField(max_length=5, blank=True,null=True)
    remarks = models.TextField(max_length=50, blank=True,null=True)
    product = models.ForeignKey(product,on_delete=models.CASCADE, related_name='product_descr')
    is_active = models.BooleanField(default=True) 
    user_created = models.ForeignKey(User,on_delete=models.CASCADE,related_name='user_created_productdetail_all')
    date_created = models.DateTimeField(auto_now_add=True)
    user_modified = models.ForeignKey(User,blank=True, null=True,on_delete=models.CASCADE,related_name='user_modified_productdetail_all')
    date_modified =models.DateTimeField(blank=True, null=True)

    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.description
    
    
class sale_product(models.Model):
    quantity = models.CharField(max_length=150,blank=True, null=True)
    product=models.ForeignKey(product, blank=True, null=True, on_delete=models.CASCADE)
    product_stock = models.ForeignKey(product_stock,blank=True, null=True,on_delete=models.CASCADE, related_name='product_stock_sale')
    seller = models.ForeignKey(seller,on_delete=models.CASCADE, related_name='seller_sale')
    city = models.ForeignKey(city,on_delete=models.CASCADE, related_name='city_sale')
    product_category = models.ForeignKey(product_category,on_delete=models.CASCADE, related_name='product_category')
    is_active = models.BooleanField(default=True) 
    order_id=models.IntegerField(blank=True,null=True)
    user_created = models.ForeignKey(User,on_delete=models.CASCADE,related_name='user_created_sale')
    date_created = models.DateTimeField(auto_now_add=True)
    user_modified = models.ForeignKey(User,blank=True, null=True,on_delete=models.CASCADE,related_name='user_modified_sale')
    date_modified = models.DateTimeField(blank=True, null=True)

    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.seller.name.username

class product_qty(models.Model):
    qty_type = models.CharField(max_length=255, blank=True,null=True)
    qty_number = models.CharField(max_length=255, blank=True,null=True)
    selled_price = models.FloatField()
    original_price = models.FloatField()
    product_price = models.ForeignKey(sale_product,on_delete=models.CASCADE, related_name='product_price')
    user_created = models.ForeignKey(User,on_delete=models.CASCADE,related_name='user_created_productqty')
    date_created = models.DateTimeField(auto_now_add=True)
 
    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.qty_type   
    
class product_offer(models.Model):
    offer_persentage = models.CharField(max_length=255, blank=True,null=True)
    product=models.ForeignKey(product, blank=True, null=True, on_delete=models.CASCADE)
    seller = models.ForeignKey(seller, blank=True, null=True, on_delete=models.CASCADE,related_name='seller_offer')
    is_active = models.BooleanField() 
    user_created = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE,related_name='user_offer')
    date_created = models.DateField(auto_now_add=True)
    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.product

class product_image(models.Model):
    name = models.CharField(max_length=255, blank=True,null=True)
    title = models.CharField(max_length=100, blank=True,null=True)
    image_type = models.CharField(max_length=255, blank=True,null=True)
    image_size = models.CharField(max_length=255, blank=True,null=True)
    is_active = models.BooleanField(default=True)
    product= models.ForeignKey(product, blank=True, null=True, on_delete=models.CASCADE)
    date_created = models.DateField(auto_now_add=True)
    image=models.ImageField(upload_to ='product/images/')
  
    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.product.full_name
    
class order(models.Model):
    latitude=models.CharField(max_length=255, blank=True,null=True)
    longitude=models.CharField(max_length=255, blank=True,null=True)
    is_active = models.BooleanField()
    date_created = models.DateField(auto_now=True)
    address=models.TextField()
    customer = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE,related_name='customer_order')
 
    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.address
    
class order_items(models.Model):
    order_status = (
        (0, 'processing'),
        (1, 'confirmed'),
        (2, 'picked'),
        (3, 'on the way'),
        (4, 'delivered'),
        
    )
    order = models.ForeignKey(order, blank=True, null=True, on_delete=models.CASCADE) 
    is_active = models.BooleanField(default=True)
    product = models.ForeignKey(product, blank=True, null=True, on_delete=models.CASCADE,related_name='product_order')
    product_qty = models.ForeignKey(product_qty, blank=True, null=True, on_delete=models.CASCADE)
    qty=models.CharField(max_length=255, blank=True,null=True)
    order_now =  models.IntegerField(choices=order_status, default='0')

    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.qty
    
class cart(models.Model):
    customer = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE,related_name='customer')
    product = models.ForeignKey(product, blank=True, null=True, on_delete=models.CASCADE,related_name='product')
    product_qty = models.ForeignKey(product_qty, blank=True, null=True, on_delete=models.CASCADE)
    qty=models.CharField(max_length=255, blank=True,null=True)
    date_created = models.DateField(auto_now=True)
    status = models.BooleanField(default=True)
    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.product.full_name
        
    
class customer_details(models.Model):
    customer = models.OneToOneField(User, blank=True, null=True, on_delete=models.CASCADE)
    image=models.ImageField(upload_to ='profile/images/',blank=True,null=True)
    phone = models.CharField(max_length=10)
    email = models.CharField(max_length=100,blank=True, null=True)
    address = models.TextField(blank=True, null=True)
    is_active = models.BooleanField(default=True) 
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(blank=True, null=True)

    class Meta:
        default_permissions = ()

    def __str__(self):
        return self.customer.username
    
    
class advertisement(models.Model):
    image=models.ImageField(upload_to ='adv/images/',blank=True,null=True)
    city=models.ForeignKey(city, blank=True, null=True, on_delete=models.CASCADE)
    shop_adv=models.BooleanField()
    product_adv=models.BooleanField()
    app_adv=models.BooleanField()
    product_id = models.ForeignKey(product, blank=True, null=True, on_delete=models.CASCADE)
    shop_id = models.ForeignKey(seller, blank=True, null=True, on_delete=models.CASCADE)
    date_created = models.DateField(auto_now=True)
    status=models.BooleanField(default=True)
    class Meta:
        default_permissions = ()

    def __str__(self):
        return str(self.date_created)