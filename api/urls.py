from django.urls import path
from api.views import (getCities, getCategories, getshops, getProducts, sellerView, myorder,
                       addToCart, viewCart, removeFroCart, buynow, register, login, UpdatePassword, profile_view,
                       UserUpdateView,
                       delivery_charge, advert, top_products, search_product, shopProductList, updateShopProductList,
                       removeProduct,
                       orderlist, updateStatus, add_product_under_shop, getSeller, getAllCategories)
                    

urlpatterns = [
    
    path('getCities/',getCities.as_view(),name="getCities"),
    path('getCategories/',getCategories.as_view(),name="getCategories"),
    path('getSeller/',getSeller.as_view(),name="getSeller"),
    path('getshops/',getshops.as_view(),name="getshops"),
    path('getProducts/',getProducts.as_view(),name="getProducts"),
    path('addToCart/',addToCart.as_view(),name="addToCart"),
    path('viewCart/',viewCart.as_view(),name="viewCart"),
    path('removeFroCart/',removeFroCart.as_view(),name="removeFroCart"),
    path('buynow/',buynow.as_view(),name="buynow"),
    path('register/',register.as_view(),name="register"),
    path('login/',login.as_view(),name="login"),
    path('UpdatePassword/',UpdatePassword.as_view(),name="UpdatePassword"),
    path('profile_view/',profile_view.as_view(),name="profile_view"),
    path('UserUpdateView/',UserUpdateView.as_view(),name="UserUpdateView"),
    path('delivery_charge/',delivery_charge.as_view(),name="delivery_charge"),
    path('advertisement/',advert.as_view(),name="advertisement"),
    path('top_products/',top_products.as_view(),name="top_products"),
    path('search_product/',search_product.as_view(),name="search_product"),
    path('sellerView/',sellerView.as_view(),name="sellerView"),
    path('myorder/',myorder.as_view(),name="myorder"),
    path('shopProductList/',shopProductList.as_view(),name="shopProductList"),
    path('updateShopProductList/',updateShopProductList.as_view(),name="updateShopProductList"),
    path('removeProduct/',removeProduct.as_view(),name="removeProduct"),
    path('orderlist/',orderlist.as_view(),name="orderlist"),
    path('updateStatus/',updateStatus.as_view(),name="updateStatus"),
    path('add_product_under_shop/',add_product_under_shop.as_view(),name="add_product_under_shop"),
    path('getAllCategories/', getAllCategories.as_view(), name="getAllCategories"),

]
