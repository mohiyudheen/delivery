# Generated by Django 3.1 on 2020-08-29 14:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_product_category_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product_category',
            name='image',
            field=models.ImageField(upload_to='media/category/images/'),
        ),
    ]
