from django.contrib import admin
from .models import (product_offer,place,nation,state,district,city,place,
                     product_category,product,product_image,seller,cart,order,customer_details,product_qty,deliver_charge,
                     advertisement,shop_category,sale_product,seller_image,product_detail,order_items)

# Register your models here.
admin.site.register(product_category)
admin.site.register(product_offer)
admin.site.register(place)
admin.site.register(nation)
admin.site.register(state)
admin.site.register(district)
admin.site.register(city)
admin.site.register(product)
admin.site.register(product_image)
admin.site.register(seller)
admin.site.register(cart)
admin.site.register(order)
admin.site.register(customer_details)
admin.site.register(product_qty)
admin.site.register(deliver_charge)
admin.site.register(advertisement)
admin.site.register(shop_category)
admin.site.register(sale_product)
admin.site.register(seller_image)
admin.site.register(product_detail)
admin.site.register(order_items)